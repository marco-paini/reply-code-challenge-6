package handsome.code.monkeys;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.solver.SolutionManager;
import org.optaplanner.core.api.solver.SolverJob;
import org.optaplanner.core.api.solver.SolverManager;

import handsome.code.monkeys.domain.Desk;
import handsome.code.monkeys.domain.Developer;
import handsome.code.monkeys.domain.Problem;
import handsome.code.monkeys.domain.ProjectManager;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import lombok.SneakyThrows;

@QuarkusMain
public class Main implements QuarkusApplication {
	private static final String userDir = String.join("/", System.getProperty("user.dir"), "src/main/resources");

	@ConfigProperty
	Optional<List<String>> fileNames;

	@Inject
	Logger log;

	@Inject
	SolutionManager<Problem, HardSoftLongScore> solutionManager;

	@Inject
	SolverManager<Problem, Path> solverManager;

	private String input(final String fileName) {
		return String.join("/", userDir, "input", fileName);
	}

	private String output(final String fileName) {
		return String.join("/", userDir, "output", fileName);
	}

	@Override
	public int run(final String... args) throws Exception {
		fileNames.map(List::stream).orElseGet(Stream::empty).map(this::input).map(Paths::get).map(this::solve)
				.map(this::write).map(solutionManager::explain).forEach(log::debug);
		return 0;
	}

	private SolverJob<Problem, Path> solve(final Path path) {
		final Path fileName = path.getFileName();
		log.infof("=== %s ===", fileName);
		return solverManager.solve(fileName, validate(new Problem(path)));
	}

	Problem validate(@Valid final Problem problem) {
		return problem;
	}

	@SneakyThrows
	private Problem write(final SolverJob<Problem, Path> solverJob) {
		final Problem finalBestSolution = solverJob.getFinalBestSolution();
		Files.write(Paths.get(output(solverJob.getProblemId().toString())),
				Stream.concat(
						finalBestSolution.getD().stream().map(Developer::getDesk).map(Optional::ofNullable)
								.map(desk -> desk.map(Desk::toString).orElse("X")),
						finalBestSolution.getM().stream().map(ProjectManager::getDesk).map(Optional::ofNullable)
								.map(desk -> desk.map(Desk::toString).orElse("X")))
						.toList());
		return finalBestSolution;
	}
}
