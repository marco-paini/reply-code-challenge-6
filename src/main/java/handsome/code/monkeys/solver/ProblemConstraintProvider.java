package handsome.code.monkeys.solver;

import java.util.Set;
import java.util.stream.Stream;

import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;

import handsome.code.monkeys.domain.Desk;
import handsome.code.monkeys.domain.Developer;
import handsome.code.monkeys.domain.ProjectManager;

public class ProblemConstraintProvider implements ConstraintProvider {
	private boolean adjacent(final Desk desk1, final Desk desk2) {
		if (desk1 == null || desk2 == null) {
			return false;
		}
		final int x1 = desk1.x;
		final int x2 = desk2.x;
		final int y1 = desk1.y;
		final int y2 = desk2.y;
		return (x1 - 1 <= x2 && x2 <= x1 + 1 && y1 == y2) || (y1 - 1 <= y2 && y2 <= y1 + 1 && x1 == x2);
	}

	private boolean adjacent(final Developer developer1, final Developer developer2) {
		return adjacent(developer1.getDesk(), developer2.getDesk());
	}

	private boolean adjacent(final Developer developer, final ProjectManager projectManager) {
		return adjacent(developer.getDesk(), projectManager.getDesk());
	}

	private boolean adjacent(final ProjectManager projectManager1, final ProjectManager projectManager2) {
		return adjacent(projectManager1.getDesk(), projectManager2.getDesk());
	}

	@Override
	public Constraint[] defineConstraints(final ConstraintFactory constraintFactory) {
		return new Constraint[] { developers(constraintFactory), developersDesk(constraintFactory),
				developersProjectManagers(constraintFactory), developersSkills(constraintFactory),
				projectManagers(constraintFactory), projectManagersDesk(constraintFactory) };
	}

	private Constraint developers(final ConstraintFactory constraintFactory) {
		return constraintFactory
				.forEachUniquePair(Developer.class, Joiners.equal(Developer::getC), Joiners.filtering(this::adjacent))
				.reward(HardSoftLongScore.ONE_SOFT, (developer1, developer2) -> developer1.getB() * developer2.getB())
				.asConstraint("Developers");
	}

	private Constraint developersDesk(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEachUniquePair(Developer.class, Joiners.equal(Developer::getDesk))
				.penalize(HardSoftLongScore.ONE_HARD).asConstraint("Developers Desk");
	}

	private Constraint developersProjectManagers(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEach(Developer.class)
				.join(ProjectManager.class, Joiners.equal(Developer::getC, ProjectManager::getC),
						Joiners.filtering(this::adjacent))
				.reward(HardSoftLongScore.ONE_SOFT,
						(developer, projectManager) -> developer.getB() * projectManager.getB())
				.asConstraint("Developers - Project Managers");
	}

	private Constraint developersSkills(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEachUniquePair(Developer.class, Joiners.filtering(this::adjacent))
				.rewardLong(HardSoftLongScore.ONE_SOFT, this::reward).asConstraint("Developers Skills");
	}

	private Constraint projectManagers(final ConstraintFactory constraintFactory) {
		return constraintFactory
				.forEachUniquePair(ProjectManager.class, Joiners.equal(ProjectManager::getC),
						Joiners.filtering(this::adjacent))
				.reward(HardSoftLongScore.ONE_SOFT,
						(projectManager1, projectManager2) -> projectManager1.getB() * projectManager2.getB())
				.asConstraint("Project Managers");
	}

	private Constraint projectManagersDesk(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEachUniquePair(ProjectManager.class, Joiners.equal(ProjectManager::getDesk))
				.penalize(HardSoftLongScore.ONE_HARD).asConstraint("Project Managers Desk");
	}

	private long reward(final Developer developer1, final Developer developer2) {
		final Set<String> s1 = developer1.getS();
		final Set<String> s2 = developer2.getS();
		final long count = s1.stream().filter(s2::contains).count();
		return (Stream.concat(s1.stream(), s2.stream()).distinct().count() - count) * count;
	}
}
