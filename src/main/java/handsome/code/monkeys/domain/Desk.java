package handsome.code.monkeys.domain;

import java.awt.Point;

import org.optaplanner.core.api.domain.lookup.PlanningId;

import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class Desk extends Point {
	private static final long serialVersionUID = 6823241955508862676L;

	public Desk(final int x, final int y) {
		super(x, y);
	}

	@Override
	@PlanningId
	public String toString() {
		return String.join(" ", String.valueOf(x), String.valueOf(y));
	}
}
