package handsome.code.monkeys.domain;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@PlanningSolution
public class Problem {
	@PlanningEntityCollectionProperty
	@Size(max = 100000, min = 10)
	private final List<Developer> D = new ArrayList<>();

	@ProblemFactCollectionProperty
	@ValueRangeProvider(id = "dDesks")
	private final List<Desk> dDesks = new ArrayList<>();

	@Max(1000)
	@Min(2)
	private int H;

	@PlanningEntityCollectionProperty
	@Size(max = 20000, min = 2)
	private final List<ProjectManager> M = new ArrayList<>();

	@ProblemFactCollectionProperty
	@ValueRangeProvider(id = "mDesks")
	private final List<Desk> mDesks = new ArrayList<>();

	@PlanningScore
	private HardSoftLongScore score;

	@Max(1000)
	@Min(2)
	private int W;

	@SneakyThrows
	public Problem(final Path path) {
		final AtomicInteger d = new AtomicInteger();
		final AtomicInteger h = new AtomicInteger();
		final AtomicInteger m = new AtomicInteger();
		Files.lines(path).forEach(line -> {
			final String[] strings = line.split(" ");
			if (H == 0) {
				H = Integer.valueOf(strings[1]);
				W = Integer.valueOf(strings[0]);
			} else if (h.get() < H) {
				final char[] charArray = strings[0].toCharArray();
				final int y = h.getAndIncrement();
				IntStream.range(0, charArray.length).forEach(x -> {
					switch (charArray[x]) {
					case '_':
						dDesks.add(new Desk(x, y));
						break;
					case 'M':
						mDesks.add(new Desk(x, y));
					}
				});
			} else if (d.get() == 0) {
				d.set(Integer.valueOf(strings[0]));
			} else if (D.size() < d.get()) {
				final Developer developer = new Developer(Integer.valueOf(strings[1]), strings[0], D.size(),
						Set.of(Arrays.copyOfRange(strings, 3, strings.length)));
				Optional.of(dDesks).filter(dd -> D.size() < dd.size()).map(dd -> dd.get(D.size()))
						.ifPresent(developer::setDesk);
				D.add(developer);
			} else if (m.get() == 0) {
				m.set(Integer.valueOf(strings[0]));
			} else {
				final ProjectManager projectManager = new ProjectManager(Integer.valueOf(strings[1]), strings[0],
						M.size());
				Optional.of(mDesks).filter(md -> D.size() < md.size()).map(md -> md.get(D.size()))
						.ifPresent(projectManager::setDesk);
				M.add(projectManager);
			}
		});
	}
}
